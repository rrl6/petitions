$(function () {
  // Grab the template script
  var theTemplateScript = $("#categories-template").html();

  // Compile the template
  var theTemplate = Handlebars.compile(theTemplateScript);

  // Define our data object
  var context=$.getJSON("name.php", function(data){ //read in data as a string?
    var temp = $.parseJSON(data);
    var outData = {
      "category": temp.category;
      "description": temp.description;
    $("#result").html(theTemplate(outData));
  });

  // Pass our data to the template
  var theCompiledHtml = theTemplate(context);

  // Add the compiled html to the page
  // $('.content-placeholder').html(theCompiledHtml);
});
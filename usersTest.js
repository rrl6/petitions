$(function () {
  // Grab the template script
  var theTemplateScript = $("#users-template").html();

  // Compile the template
  var theTemplate = Handlebars.compile(theTemplateScript);

  // Define our data object
  var context=$.getJSON("name.php", function(data){ //read in data as a string?
    var temp = $.parseJSON(data);
    var outData = {
      "netid": temp.netid;
      "lastvote": temp.lastvote;
      "remainingVotes": temp.remainingVotes;
      "admin": temp.admin;
    }
    $("#result").html(theTemplate(outData));
  });

  // Pass our data to the template
  var theCompiledHtml = theTemplate(context);

  // Add the compiled html to the page
  // $('.content-placeholder').html(theCompiledHtml);
});
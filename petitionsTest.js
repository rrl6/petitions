$(function () {
  // Grab the template script
  var theTemplateScript = $("#petitions-template").html();

  // Compile the template
  var theTemplate = Handlebars.compile(theTemplateScript);

  // Define our data object
  var context=$.getJSON("name.php", function(data){ //read in data as a string?
    var temp = $.parseJSON(data);
    var outData = {
      "id": temp.id;
      "name": temp.name;
      "author": temp.author;
      "blurb": temp.blurb;
      "content": temp.content;
      "response": temp.response;
      "tags": temp.tags;
      "category": temp.category;
      "count": temp.count;
      "date": temp.data;
    }
    $("#result").html(theTemplate(outData));
  });

  // Pass our data to the template
  var theCompiledHtml = theTemplate(context);

  // Add the compiled html to the page
  // $('.content-placeholder').html(theCompiledHtml);
});